using System.Collections;
using System.Linq;
using Zipcodes.Bootstrap;
using Zipcodes.Models;
using Zipcodes.Services.Contracts;
using Zipcodes.Utils;

namespace Zipcodes.Services
{
    public class CountriesService : ICountriesService
    {
        private readonly ZipcodesContext _context;
        
        public CountriesService(ZipcodesContext context)
        {
            _context = context;
        }

        public Country GetCountryByIsoCode2(string code2)
        {
            return _context.Countries.SingleOrDefault(country => country.Code2 == code2);
        }

        public IEnumerable SearchCountriesWithName(string name)
        {
            return _context.Countries.Where(c => c.Name.ToLower().FlexContains(name.ToLower()));
        }

        public IEnumerable GetAllCountries()
        {
            return _context.Countries;
        }
    }
}