using System.Collections;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Zipcodes.Bootstrap;

using Zipcodes.Models;
using Zipcodes.Services.Contracts;
using Zipcodes.Utils;

namespace Zipcodes.Services
{
    public class CitiesService : ICitiesService
    {
        private readonly ZipcodesContext _context;

        public CitiesService(ZipcodesContext context)
        {
            _context = context;
        }

        public City GetCityById(int id)
        {
            return _context.Cities
                .Include(c => c.State)
                .ThenInclude(s => s.Country)
                .SingleOrDefault(c => c.Id == id);
        }
        
        public IEnumerable GetCitiesByQuery(string name, string key, string stateId)
        {
            IQueryable<City> query = _context.Cities;

            if (name != null)
                query = query.Where(c => c.Name.ToLower().FlexContains(name.ToLower()));
            if (key != null)
                query = query.Where(c => c.Key.ToLower().FlexContains(key.ToLower()));
            if (int.TryParse(stateId, out var id))
                query = query.Where(c => c.State.Id == id);

            return query
                .Include(c => c.State)
                .ThenInclude(s => s.Country)
                .ToList();
        }

        public IEnumerable GetAllCities()
        {
            return _context.Cities
                .Include(c => c.State)
                    .ThenInclude(s => s.Country)
                .ToList();
        }
    }
}