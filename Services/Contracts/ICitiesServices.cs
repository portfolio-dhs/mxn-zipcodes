using System.Collections;
using Zipcodes.Models;

namespace Zipcodes.Services.Contracts
{
    public interface ICitiesService
    {
        City GetCityById(int id);
        IEnumerable GetCitiesByQuery(string name, string key, string stateId);
        IEnumerable GetAllCities();
    }
}