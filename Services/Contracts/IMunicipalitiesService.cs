using System.Collections;
using Zipcodes.Models;

namespace Zipcodes.Services.Contracts
{
    public interface IMunicipalitiesService
    {
        Municipality GetMunicipalityById(int id);
        IEnumerable GetAllMunicipalities();
        IEnumerable GetMunicipalitiesByQuery(string key, string name, string stateId);
    }
}