using System.Collections;
using Zipcodes.Models;

namespace Zipcodes.Services.Contracts
{
    public interface ICountriesService
    {
        Country GetCountryByIsoCode2(string code2);
        IEnumerable SearchCountriesWithName(string name);
        IEnumerable GetAllCountries();
    }
}