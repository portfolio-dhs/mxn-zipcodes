using System.Collections;
using Zipcodes.Models;

namespace Zipcodes.Services.Contracts
{
    public interface IStatesService
    {
        State GetStateById(int id);
        IEnumerable GetStatesByQuery(string key, string name, string code2 , string countryCode2);
        IEnumerable GetAllStates();
    }
}