using System.Collections;
using Zipcodes.Models;

namespace Zipcodes.Services.Contracts
{
    public interface IColoniesService
    {
        Colony GetColonyWithId(int id);
        IEnumerable GetColoniesByQuery(string zipcode, string name, 
            string municipalityId, string cityId);
        IEnumerable GetAllColonies();
    }
}