using System.Collections;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Zipcodes.Bootstrap;
using Zipcodes.Models;
using Zipcodes.Services.Contracts;
using Zipcodes.Utils;

namespace Zipcodes.Services
{
    public class StatesService : IStatesService
    {
        private readonly ZipcodesContext _context;

        public StatesService(ZipcodesContext context)
        {
            _context = context;
        }

        public State GetStateById(int id)
        {
            return _context.States
                .Include(s => s.Country)
                .SingleOrDefault(state => state.Id == id);
        }

        public IEnumerable GetStatesByQuery(string key, string name, string code2, string country)
        {
            IQueryable<State> query = _context.States;

            if (key != null)
                query = query.Where(s => s.Key == key);
            if (name != null)
                query = query.Where(s => s.Name.ToLower().FlexContains(name.ToLower()));
            if (code2 != null)
                query = query.Where(s => s.Code2 == code2);
            if (country != null)
                query = query.Where(s => s.Country.Code2 == code2);

            return query
                .Include(s => s.Country)
                .ToList();
        }

        public IEnumerable GetAllStates()
        {
            return _context.States.ToList();
        }
    }
}