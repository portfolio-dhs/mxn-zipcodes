using System.Collections;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Zipcodes.Bootstrap;
using Zipcodes.Models;
using Zipcodes.Services.Contracts;
using Zipcodes.Utils;

namespace Zipcodes.Services
{
    public class MunicipalitiesService : IMunicipalitiesService
    {
        private readonly ZipcodesContext _context;

        public MunicipalitiesService(ZipcodesContext context)
        {
            _context = context;
        }

        public Municipality GetMunicipalityById(int id)
        {
            return _context.Municipalities
                .Include(m => m.State)
                    .ThenInclude(s => s.Country)
                .SingleOrDefault(m => m.Id == id);
        }

        public IEnumerable GetMunicipalitiesByQuery(string key, string name, string stateId)
        {
            IQueryable<Municipality> query = _context.Municipalities;

            if (key != null)
                query = query.Where(m => m.Key == key);
            if (name != null)
                query = query.Where(m => m.Name.ToLower().FlexContains(name.ToLower()));
            if (int.TryParse(stateId, out var sid))
                query = query.Where(m => m.State.Id == sid);

            return query
                .Include(m => m.State)
                    .ThenInclude(s => s.Country)
                .ToList();
        }

        public IEnumerable GetAllMunicipalities()
        {
            return _context.Municipalities
                .Include(m => m.State)
                .ThenInclude(s => s.Country);
        }
    }
}