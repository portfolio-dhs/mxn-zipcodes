using System.Collections;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using Zipcodes.Bootstrap;
using Zipcodes.Models;
using Zipcodes.Services.Contracts;
using Zipcodes.Utils;

namespace Zipcodes.Services
{
    public class ColoniesService : IColoniesService
    {
        private readonly ZipcodesContext _context;

        public ColoniesService(ZipcodesContext context)
        {
            _context = context;
        }
        
        public Colony GetColonyWithId(int id)
        {
            return _context.Colonies
                .Include(c => c.Municipality)
                    .ThenInclude(m => m.State)
                    .ThenInclude(s => s.Country)
                .Include(c => c.City)
                    .ThenInclude(m => m.State)
                    .ThenInclude(s => s.Country)
                .SingleOrDefault(c => c.Id == id);
        }

        public IEnumerable GetColoniesByQuery(string zipcode, string name, string municipalityId, string cityId)
        {
            IQueryable<Colony> query = _context.Colonies;

            if (int.TryParse(zipcode, out var code)) 
                query = query.Where(c => c.PostalCode == code);
            if (name != null)
                query = query.Where(c => c.Name.ToLower().FlexContains(name.ToLower()));
            if (int.TryParse(municipalityId, out var mid))
                query = query.Where(c => c.Municipality.Id == mid);
            if (int.TryParse(cityId, out var cid))
                query = query.Where(c => c.City.Id == cid);

            return query
                .Include(c => c.Municipality)
                    .ThenInclude(m => m.State)
                    .ThenInclude(s => s.Country)
                .Include(c => c.City)
                    .ThenInclude(m => m.State)
                    .ThenInclude(s => s.Country)
                .ToList();
        }

        public IEnumerable GetAllColonies()
        {
            return _context.Colonies
                .Include(c => c.Municipality)
                    .ThenInclude(m => m.State)
                    .ThenInclude(s => s.Country)
                .Include(c => c.City)
                    .ThenInclude(m => m.State)
                    .ThenInclude(s => s.Country)
                .ToList();
        }
    }
}