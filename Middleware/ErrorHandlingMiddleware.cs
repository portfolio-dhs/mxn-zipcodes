using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using Zipcodes.Exceptions;

namespace Zipcodes.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;

        public ErrorHandlingMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private static Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = GetCode(exception);

            var error = new
            {
                status = code,
                message = exception.Message
            };

            var result = JsonConvert.SerializeObject(new { error });

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = code;

            return context.Response.WriteAsync(result);
        }

        private static int GetCode(Exception exception)
        {
            var code = GetStatusExceptionBased(exception);

            return (int)code;
        }

        private static HttpStatusCode GetStatusExceptionBased(Exception exception)
        {
            switch (exception)
            {
                case UnauthorizedException _:
                    return HttpStatusCode.Unauthorized;
                case NotFoundException _:
                    return HttpStatusCode.NotFound;
                case ConflictException _:
                    return HttpStatusCode.Conflict;
                case FailedConfirmationException _:
                    return HttpStatusCode.Forbidden;
                default:
                    return HttpStatusCode.InternalServerError;
            }
        }
    }
}