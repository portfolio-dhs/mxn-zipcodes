using System.Globalization;

namespace Zipcodes.Utils
{
    public static class StringExtensions
    {
        public static bool FlexContains(this string text, string value)
        {   
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;

            var index = compareInfo.IndexOf(text, value,
                CompareOptions.IgnoreCase
                | CompareOptions.IgnoreSymbols
                | CompareOptions.IgnoreNonSpace);

            return index >= 0;
        }
    }
}