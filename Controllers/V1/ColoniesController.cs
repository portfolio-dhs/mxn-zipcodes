using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Zipcodes.Exceptions;
using Zipcodes.Services;
using Zipcodes.Services.Contracts;

namespace Zipcodes.Controllers.V1
{
    [Route("v1/colonies")]
    public class ColoniesController : ControllerBase
    {
        private readonly IColoniesService _coloniesService;

        public ColoniesController(IColoniesService coloniesService)
        {
            _coloniesService = coloniesService;
        }

        [HttpGet("{id}")]
        public IActionResult GetCityWithId(int? id)
        {
            if (!id.HasValue)
                throw new BadRequestException();

            var colony = _coloniesService.GetColonyWithId(id.Value);

            if (colony == null)
                throw new NotFoundException();

            return Ok(colony);
        }

        // TODO: Add pagination to this. #6
        [HttpGet]
        public IActionResult GetCitiesByQuery(
            [FromQuery(Name = "zipcode")] string zipcode,
            [FromQuery(Name = "name")] string name,
            [FromQuery(Name = "municipality-id")] string municipalityId,
            [FromQuery(Name = "city-id")] string cityId)
        {
            IEnumerable colonies;
            if (string.IsNullOrWhiteSpace(zipcode) && 
                string.IsNullOrWhiteSpace(name) &&
                string.IsNullOrWhiteSpace(municipalityId) &&
                string.IsNullOrWhiteSpace(cityId))
            {
                colonies = _coloniesService.GetAllColonies();
                
                if (!colonies.Any())
                    throw new NotFoundException();

                return Ok(colonies);
            }

            colonies = _coloniesService.GetColoniesByQuery(zipcode, name, municipalityId, cityId);

            if (!colonies.Any())
                throw new NotFoundException();

            return Ok(colonies);
        }
    }
}