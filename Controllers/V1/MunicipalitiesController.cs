using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Zipcodes.Exceptions;
using Zipcodes.Services;
using Zipcodes.Services.Contracts;

namespace Zipcodes.Controllers.V1
{
    [Route("v1/municipalities")]
    public class MunicipalitiesController : ControllerBase
    {
        private readonly IMunicipalitiesService _municipalitiesService;

        public MunicipalitiesController(IMunicipalitiesService municipalitiesService)
        {
            _municipalitiesService = municipalitiesService;
        }

        [HttpGet("{id}")]
        public IActionResult GetMunicipalityById(int ?id)
        {
            if (!id.HasValue)
                throw new BadRequestException();

            var municipality = _municipalitiesService.GetMunicipalityById(id.Value);

            if (municipality == null)
                throw new NotFoundException();

            return Ok(municipality);
        }

        // TODO: Add pagination to this. #6
        [HttpGet]
        public IActionResult GetMunicipalitiesByQuery(
            [FromQuery(Name = "key")] string key,
            [FromQuery(Name = "name")] string name,
            [FromQuery(Name = "state-id")] string stateId)
        {
            IEnumerable municipalities;

            if (string.IsNullOrWhiteSpace(key) &&
                string.IsNullOrWhiteSpace(name) &&
                string.IsNullOrWhiteSpace(stateId))
            {
                municipalities = _municipalitiesService.GetAllMunicipalities();

                if (!municipalities.Any())
                    throw new NotFoundException();
            
                return Ok(_municipalitiesService.GetAllMunicipalities());   
            }

            municipalities = _municipalitiesService.GetMunicipalitiesByQuery(key, name, stateId);

            if (!municipalities.Any())
                throw new NotFoundException();

            return Ok(municipalities);
        }
    }
}