using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Zipcodes.Exceptions;
using Zipcodes.Services;
using Zipcodes.Services.Contracts;

namespace Zipcodes.Controllers.V1
{
    [Route("v1/countries")]
    public class CountriesController : ControllerBase
    {
        private readonly ICountriesService _countriesService;

        public CountriesController(ICountriesService countriesService) 
        {
            _countriesService = countriesService;
        }

        [HttpGet("code/{code}")]
        public IActionResult GetCountryWithIsoCode2(string code)
        {
            if (string.IsNullOrWhiteSpace(code)) 
                throw new BadRequestException();
            
            var country = _countriesService.GetCountryByIsoCode2(code);

            if (country == null)
                throw new NotFoundException();

            return Ok(country);
        }

        [HttpGet]
        public IActionResult GetAll([FromQuery(Name = "name")] string name)
        {
            IEnumerable countries;

            if (name == null)
            {
                countries = _countriesService.GetAllCountries();
            
                if (!countries.Any())
                    throw new NotFoundException();

                return Ok(countries);
            }

            countries = _countriesService.SearchCountriesWithName(name);

            if (!countries.Any())
                throw new NotFoundException();
            
            return Ok(countries);
        }
    }
}