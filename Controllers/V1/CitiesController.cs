using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Zipcodes.Exceptions;
using Zipcodes.Services.Contracts;

namespace Zipcodes.Controllers.V1
{
    [Route("v1/cities")]
    public class CitiesController : ControllerBase
    {
        private readonly ICitiesService _citiesService;

        public CitiesController(ICitiesService citiesService)
        {
            _citiesService = citiesService;
        }

        [HttpGet("{id}")]
        public IActionResult GetCityById(int ?id)
        {
            if (!id.HasValue)
                throw new BadRequestException();

            var city = _citiesService.GetCityById(id.Value);

            if (city == null)
                throw new NotFoundException();

            return Ok(city);
        }

        // TODO: Add pagination to this. #6
        [HttpGet]
        public IActionResult GetCitiesByQuery(
            [FromQuery(Name = "key")] string key,
            [FromQuery(Name = "name")] string name,
            [FromQuery(Name = "state")] string state)
        {
            IEnumerable cities;
            
            if (string.IsNullOrWhiteSpace(key) && 
                string.IsNullOrWhiteSpace(name) &&
                string.IsNullOrWhiteSpace(state))
            {
                cities = _citiesService.GetAllCities();

                return Ok(cities);
            }

            cities = _citiesService.GetCitiesByQuery(name, key, state);

            if (!cities.Any())
                throw new NotFoundException();

            return Ok(cities);
        }
    }
}