using System.Collections;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Zipcodes.Exceptions;
using Zipcodes.Services.Contracts;

namespace Zipcodes.Controllers.V1
{
    [Route("v1/states")]
    public class StatesController : ControllerBase
    {
        private readonly IStatesService _statesService;

        public StatesController(IStatesService statesService)
        {
            _statesService = statesService;
        }

        [HttpGet("{id}")]
        public IActionResult GetStateById(int? id)
        {
            if (!id.HasValue)
                throw new BadRequestException();

            var state = _statesService.GetStateById(id.Value);

            if (state == null)
                throw new NotFoundException();

            return Ok(state);
        }

        [HttpGet]
        public IActionResult GetAllStates(
            [FromQuery(Name = "key")] string key,
            [FromQuery(Name = "name")] string name,
            [FromQuery(Name = "code2")] string code2,
            [FromQuery(Name = "country-code2")] string countryCode2)
        {
            IEnumerable states;

            if (string.IsNullOrWhiteSpace(key) &&
                string.IsNullOrWhiteSpace(name) &&
                string.IsNullOrWhiteSpace(code2) &&
                string.IsNullOrWhiteSpace(countryCode2))
            {
                states = _statesService.GetAllStates();

                if (!states.Any())
                    throw new NotFoundException();

                return Ok(states);
            }

            states = _statesService.GetStatesByQuery(key, name, code2, countryCode2);

            if (!states.Any())
                throw new NotFoundException();

            return Ok(states);
        }
    }
}