# Changelog
## [2.2.0](https://gitlab.com/devianlabs/zipcodes/tree/v2.2.0) - 2019-12-06
### Added
- Added Sentry error tracker support:
  - To add the sentry you must set the DSN through an environment variable
  called SENTRY_DSN. Example: 
  ```set -Ux SENTRY_DSN="https://example.com/dsn/123asd"```.
### Changed
- Database connection string.
  - Now the connection String is passed via environment variable.
  Example ```set -Ux DB_CONNECTION="Host=db.example.com;Port=5432;Database=db;User Id=user;Password=password>"```.
- Cleaned up some code.
### Deleted
- Deleted Newtonsoft.Json library from the project.
  - Is no longer needed.
## [2.1.0](https://gitlab.com/devianlabs/zipcodes/tree/v2.1.0) - 2019-12-02
### Added
- Search ignoring symbols like accents (with 
Zipcodes.Utils.StringExtensions.FlexContaines()).
## [2.0.0](https://gitlab.com/devianlabs/zipcodes/tree/v2.0.0) - 2019-11-28
Resolves: #2
### Changed
- Moved API endpoints under v1.
- Changed endpoints to be compliant with REST.
- Changed the middleware to be more flexible.
- Fixed the dates of the changelog.
### Deleted
- Deleted all the specific query services like ColoniesService.SearchByName()
and created one service method fo handling this kind of search-like
queries.
- Deleted HTTPS redirection because the web server wil be the one that
manages the HTTP/HTTPS connections with a reverse proxy.
## [1.1.1](https://gitlab.com/devianlabs/zipcodes/tree/v1.1.1) - 2019-11-27
Hotfix
### Added
- Added docker-compose.yml for easy deployment with docker compose.
### Changed
- Changed db connection address to zb.zipcodes.devianlabs.com in appsetings.json.
- Changed Dockerfile entrypoint for fixing a docker initialization bug.
- Changed default ports to 80 and 443 from Bootstrap/Program.cs.
## [1.1.0](https://gitlab.com/devianlabs/zipcodes/tree/v1.1.0) - 2019-11-27
Added Docker Support
**Database dockerfile and SQL backup script is under 
google drive "DevianDocs/Projects/\[DevianLabs\]Zipcodes"**
## [1.0.1](https://gitlab.com/devianlabs/zipcodes/tree/v1.0.1) - 2019-11-26
Cleanup Refactory:
### Added:
- Added ErrorHandlingMiddleware for HTTP error handling.
### Changed
- Changed expresion-like bodies for conventional statement-like bodies.
- Changed braces to brace-less IF statements.
- Deleted semantic whitespaces.
- Changed name of variables for more meaningful ones.
- Changed namespace of the project.
- Changed from manual string inspection to string.IsNullOrWhitespace().
## [1.0.0](https://gitlab.com/devianlabs/zipcodes/tree/v1.0.0) - 2019-11-25
### Added
- Added first API endpoints.
