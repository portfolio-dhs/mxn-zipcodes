namespace Zipcodes.Exceptions
{
    public class BadRequestException : AppException
    {
        public BadRequestException() : base("The request is malformed.") { }
        public BadRequestException(string message) : base(message) { }
    }
}