namespace Zipcodes.Exceptions
{
    public class NotConfiguredException : AppException
    {
        public NotConfiguredException() : base("Server configuration is missing.") { }
        public NotConfiguredException(string message) : base(message) { }
    }
}