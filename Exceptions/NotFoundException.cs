namespace Zipcodes.Exceptions
{
    public class NotFoundException : AppException
    {
        public NotFoundException() : base("The requested resource could not be found") { }
        public NotFoundException(string message) : base(message) { }
    }
}