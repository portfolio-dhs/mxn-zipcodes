using System;
using System.Globalization;

namespace Zipcodes.Exceptions
{
    public class AppException : Exception
    {
        public AppException() : base() { }

        protected AppException(string message) : base(message) { }

        public AppException(string message, params object[] args)
            : base(string.Format(CultureInfo.CurrentCulture, message, args)) { }
    }
}