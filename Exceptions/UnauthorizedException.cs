namespace Zipcodes.Exceptions
{
    public class UnauthorizedException : AppException
    {
        public UnauthorizedException() : base("Please, reauthenticate and try again") { }
        public UnauthorizedException(string message) : base(message) { }
    }
}