namespace Zipcodes.Exceptions
{
    public class FailedConfirmationException : AppException
    {
        public FailedConfirmationException()
          : base("The request was valid, but the server is refusing action. The user might not have the necessary permissions for a resource.") { }
        public FailedConfirmationException(string message) : base(message) { }
    }
}