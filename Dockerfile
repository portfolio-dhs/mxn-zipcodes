FROM mcr.microsoft.com/dotnet/core/sdk:3.0 AS build-env
WORKDIR /app

# Copy csproj and restore as distinct layers
COPY mxn-zipcodes.csproj ./
RUN dotnet restore

# Copy everything else and build
COPY . .
RUN dotnet publish -c Release -o out/

# Run
FROM mcr.microsoft.com/dotnet/core/aspnet:3.0
WORKDIR /srv
COPY --from=build-env /app/out .
ENTRYPOINT ["dotnet", "mxn-zipcodes.dll"]