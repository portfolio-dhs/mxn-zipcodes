using System.ComponentModel.DataAnnotations;

namespace Zipcodes.Models
{
    public class Country
    {
        [Key] [Required] public string Code2 { get; set; }
        [Required] public string Name { get; set; }
    }
}