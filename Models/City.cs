using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Zipcodes.Models
{
    public class City
    {
        [Key] public int Id { get; set; }
        [Required] public string Key { get; set; }
        [Required] public string Name { get; set; }
        [ForeignKey("state_id")] public State State { get; set; }
    }
}