using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Zipcodes.Models
{
    public class Colony
    {
        [Key] public int Id { get; set; }
        public int PostalCode { get; set; }
        [Required] public string Name { get; set; }
        [ForeignKey("municipality_id")]public Municipality Municipality { get; set; }
        [ForeignKey("city_id")] public City City { get; set; }
    }
}