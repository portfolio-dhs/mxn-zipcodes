using System.ComponentModel.DataAnnotations;

namespace Zipcodes.Models
{
    public class Municipality
    {
        [Key]
        public int Id { get; set; }
        [Required] public string Key { get; set; } 
        [Required] public string Name { get; set; }
        public int StateId { get; set; }
        public virtual State State { get; set; }
    }
}