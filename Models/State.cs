using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Zipcodes.Models
{
    public class State
    {
        [Key] public int Id { get; set; }
        [Required] public string Key { get; set; }
        [Required] public string Name { get; set; }
        public string Code2 { get; set; }
        [ForeignKey("country")] public Country Country { get; set; }
    }
}