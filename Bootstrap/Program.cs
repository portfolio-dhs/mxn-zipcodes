using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;

namespace Zipcodes.Bootstrap
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        private static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder
                        .UseSentry(Environment.GetEnvironmentVariable("SENTRY_DSN"))
                        .UseStartup<StartupConfig>()
                        .UseKestrel()
                        .UseUrls("http://*:5000"); 
                });    
    }
}
