using Microsoft.EntityFrameworkCore;
using Zipcodes.Models;

namespace Zipcodes.Bootstrap
{
    public class ZipcodesContext : DbContext
    {
        public DbSet<City> Cities { get; set; }
        public DbSet<Colony> Colonies { get; set; }
        public DbSet<Country> Countries { get; set; }
        public DbSet<Municipality> Municipalities { get; set; }
        public DbSet<State> States { get; set; }


        public ZipcodesContext(DbContextOptions<ZipcodesContext> options) : base(options) { }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.FixSnakeCaseNames();
            
        }
    }
}