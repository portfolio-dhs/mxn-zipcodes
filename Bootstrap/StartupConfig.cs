using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Zipcodes.Exceptions;
using Zipcodes.Middleware;
using Zipcodes.Services;
using Zipcodes.Services.Contracts;

namespace Zipcodes.Bootstrap
{
    public class StartupConfig
    {
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc();
            services.AddEntityFrameworkNpgsql().AddDbContext<ZipcodesContext>(options =>
                options.UseNpgsql(Environment.GetEnvironmentVariable("DB_CONNECTION") 
                                    ?? throw new NotConfiguredException()));
            services.AddScoped<ICountriesService, CountriesService>();
            services.AddScoped<IStatesService, StatesService>();
            services.AddScoped<IMunicipalitiesService, MunicipalitiesService>();
            services.AddScoped<ICitiesService, CitiesService>();
            services.AddScoped<IColoniesService, ColoniesService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        // ReSharper disable once UnusedMember.Global
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware(typeof(ErrorHandlingMiddleware));
            app.UseRouting();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
