Zipcodes is an API for translating zipcodes into a full address 
and vice versa.

## Installation
#### IMPORTANT: Deploy the db before executing

Install docker and run:
```
docker run -p 5000:5000 registry.gitlab.com/portfolio-dhs/mxn-zipcodes/master
```

For building use:
```
dotnet restore && dotnet publish -c Release -o out
```

If you want to build the Docker run:
```
docker build -t mxn-zipcodes .
```

## Usage
Endpoints:
- /v1/countries:
  - Path Params: 
    - /code/{id}: The ISO code of the country.
- /v1/states:
  - Path Params:
    - {id}: The state DB id.
  - Query Params:
    - key: Special zipcode key.
    - name: Name of the city.
    - code2: The state code-2.
    - country-code2: The country code-2.
- /v1/municipalities/
  - Path Params:
    - {id}: The municipality DB id.
  - Query params: 
    - key: Special zipcode key.
    - name: Name of the municipality.
    - state-id: The state DB id.
- /v1/cities:
  - Path Params:
    - {id}: The city DB id.
  - Query Params:
    - key: Special zipcode key.
    - name: Name of the city.
    - state-id: The state DB id.
- /v1/colonies:
  - Path Params:
    - {id}: The municipality DB id.
  - Query Params:
    - zipcode: The zipcode.
    - name: The colony name.
    - municipality-id: The municipality DB id.
    - city-id: The city DB id.